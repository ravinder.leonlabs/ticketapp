package com.sap.ticket.view;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TicketView {
	
	public String destinationFrom;
	
	public String destinationTo;
	
	public float distance;
	
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TicketView other = (TicketView) obj;
		if (destinationFrom == null) {
			if (other.destinationFrom != null)
				return false;
		} else if (!destinationFrom.equals(other.destinationFrom))
			return false;
		if (destinationTo == null) {
			if (other.destinationTo != null)
				return false;
		} else if (!destinationTo.equals(other.destinationTo))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destinationFrom == null) ? 0 : destinationFrom.hashCode());
		result = prime * result + ((destinationTo == null) ? 0 : destinationTo.hashCode());
		return result;
	}

	public TicketView(String destinationFrom, String destinationTo) {
		super();
		this.destinationFrom = destinationFrom;
		this.destinationTo = destinationTo;
	}

}
