package com.sap.ticket.constant;

public class CommonConstants {
	
	
	public static String BOOLEAN_TRUE 			= "true";
	public static String BOOLEAN_FALSE 			= "false";
	public static String STATUS_SUCCESS 		= "Success";
	public static String STATUS_FAILURE 		= "Failure";
	public static String SYSTEM_ERROR 			= "SYSTEM_ERROR";
	
	public static String JSON_DILIMITER 		= ":";
	
}
