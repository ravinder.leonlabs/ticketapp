package com.sap.ticket.rest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sap.ticket.exception.RequestValidationException;
import com.sap.ticket.service.TicketService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/ticket")
@Api(tags = { "ticket-service" })
public class TicketController extends AbstractRestHandler {

	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private MessageSource messages;

	@RequestMapping(value = "/calculate-fare", method = RequestMethod.GET, produces = { "application/json" })
	@ApiOperation( value = "Calculate fare for the specified destination", response = JSONObject.class)
	public Double reArrangeString(@RequestParam("src") String src, @RequestParam("dest") String dest) throws Exception {
		Double fare =null;
		try {
			fare = ticketService.calculateFare(src, dest);
		} catch (RequestValidationException e) {
			throw new RequestValidationException(messages.getMessage("err.invalid.payload.type", null, null));
		}
		return fare;
	}
}
