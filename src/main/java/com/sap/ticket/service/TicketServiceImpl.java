package com.sap.ticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sap.ticket.exception.ApplicationException;
import com.sap.ticket.helper.TicketHelper;

@Service
public class TicketServiceImpl implements TicketService {
	
	@Autowired
	TicketHelper ticketHelper;
	
	public Double calculateFare(String source, String dest) throws ApplicationException {
		return ticketHelper.getFare(source, dest);
	}

}
