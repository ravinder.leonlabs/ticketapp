package com.sap.ticket.service;

import com.sap.ticket.exception.ApplicationException;

public interface TicketService {
	
	public Double calculateFare(String source, String dest) throws ApplicationException;

}
