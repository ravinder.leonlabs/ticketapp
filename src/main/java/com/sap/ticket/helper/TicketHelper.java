package com.sap.ticket.helper;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

import com.sap.ticket.exception.ApplicationException;
import com.sap.ticket.view.TicketView;

@Component
public class TicketHelper {
	
	public static List<TicketView> list = new ArrayList<TicketView>();
	
	
	@Autowired
    CacheManager cacheManager;
	
	@PostConstruct
    public void init() {
		populateDefaults();
    }
	
	private void populateDefaults() {
		list.add(new TicketView("PUN", "MUM", 120));
		list.add(new TicketView("PUN", "NAS", 200));
		list.add(new TicketView("MUM", "GOA", 350));
		list.add(new TicketView("MUM", "NAS", 180));
		list.add(new TicketView("PUN", "BLR", 880));
		list.add(new TicketView("BLR", "HYD", 900));
	}

	public double getFare(String source, String dest) throws ApplicationException {
		
		new TicketView(source, dest);
		int indexOf = list.indexOf(new TicketView(source, dest));
		if(indexOf==-1) {
			throw new ApplicationException("Travel distance not configured for specified source and destination");
		}
		float distance  = list.get(indexOf).getDistance();
		return (distance < 100 ? Double.sum(0.0d, 750.0d) : Double.sum(750.0d, ((distance - 100)*5)));
		
	}

}
